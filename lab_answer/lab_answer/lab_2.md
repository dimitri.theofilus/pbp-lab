1. Apakah perbedaan antara JSON dan XML?
-JSON hanyalah format yang ditulis dalam JavaScript sedangkan XML adalah bahasa markup, bukan bahasa pemrograman, yang memiliki tag untuk mendefinisikan elemen.
-Pada jenis data dalam JSON dan XML JSON mendukung tipe data teks dan angka termasuk bilangan bulat (mendukung data terstruktur yang menggunakan array dan objek) sedangkan XML tidak memiliki dukungan langsung untuk data yang bertipe array tetapi mendukung banyak tipe data seperti angka, teks, gambar, grafik, grafik, dan masih banyak lagi.
-Dalam sintaksnya JSON tidak memiliki tag awal dan akhir lalu sintaksisnya lebih ringan dibandingkan dengan XML karena JSON berorientasi pada data dengan redundansi yang lebih sedikit hal tersebut membuat JSON menjadi ideal untuk menukar data melalui XML. Sedangkan XML membutuhkan membutuhkan lebih banyak karakter untuk mewakili data yang sama.
-JSON memiliki tujuan untuk menjadi format file yang sederhana dan ringan untuk pertukaran data sedangkan XML memiliki tujuan untuk menjadi format standar terbuka yang sudah terdokumentasi dengan baik lalu didalamnya berisi aturan yang berbicara tentang bagaimana cara menyandingkan dokumen tersebut namun dalam format yang dapat dibaca oleh manusia dan oleh mesin.

2. Apakah perbedaan antara HTML dan XML?
-Dalam HTML memfokuskan dalam penyajian data sedangkan XML memfokuskan dalam transfer data
-Pada kegunaannya HTML dibutuhkan untuk membangun struktur halaman web sedangkan XML membantu untuk bertukar data diantara berbagai platform
-HTML tidak peka terhadap huruf besar/huruf kecil sedangkan XML peka terhadap hal tersebut.
-HTML berfokus dalam penampilan data sedangkan XML berfokus kepada membawa informasi

## Referensi :
https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://perbedaan.budisma.net/perbedaan-html-dan-xml.html
https://blogs.masterweb.com/perbedaan-xml-dan-html/