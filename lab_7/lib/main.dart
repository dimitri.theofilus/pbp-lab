// ignore_for_file: prefer_const_constructors

//referensi diambil dari https://github.com/JohannesMilke/card_example

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String title = 'Homemed Homepage';

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: title,
        theme: ThemeData(primarySwatch: Colors.cyan),
        home: MainPage(title: title),
      );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _MainPageStateIndex = 0;

  final _formKey = GlobalKey<FormState>();
  String textFieldsValue = "";

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ListView(
          padding: EdgeInsets.all(16),
          children: [
            Container(
              child: productDummy1(),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
            ),
            Container(
              child: productDummy2(),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
            ),
            Container(
              child: productDummy3(),
              margin: EdgeInsets.fromLTRB(0, 0, 10, 10),
            ),
            Container(
              child: Text('Join Us',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  )),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
            ),
            
           Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        autofocus: true,
                        keyboardType: TextInputType.url,
                        maxLength: 200,
                        decoration: InputDecoration(
                            hintText: "Enter your email here",
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8),
                            )),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Insert your email correctly!';
                          }
                          textFieldsValue = value;
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 14,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          textStyle: const TextStyle(fontSize: 20),
                          primary: Colors.orange.shade800,
                          onPrimary: Colors.white,
                          side: BorderSide(width: 2, color: Colors.transparent),
                          padding: EdgeInsets.only(
                              left: 14, right: 14, top: 7, bottom: 7),
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(20.0)),
                        ),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            print(textFieldsValue);
                          }
                        },
                        child: const Text('Post'),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
          ],
        ),

        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_shopping_cart),
              label: 'Products',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.info),
              label: 'About',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.support_agent),
              label: 'Customer Service',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle),
              label: 'Account',
            ),
          ],
          currentIndex: _MainPageStateIndex,
          selectedItemColor: Colors.cyan,
          onTap: (index) {
            setState(() {
              _MainPageStateIndex = index;
            });
          },
        ),
      );

  Widget productDummy1() => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://m.media-amazon.com/images/I/61+-lxwiE6S._AC_SL1000_.jpg',
                  ),
                  height: 240,
                  fit: BoxFit.contain,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    'ZyeZoo Disposable Face Mask : Rp.70.000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
            )),
          ],
        ),
      );

  Widget productDummy2() => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://cf.shopee.co.id/file/6f956b518c8ed0bef25915559cc1839c',
                  ),
                  height: 240,
                  fit: BoxFit.contain,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    'Kursi Roda Stainless Steel One Med: Rp.1.350.000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
            )),
          ],
        ),
      );

  Widget productDummy3() => Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        child: Column(
          children: [
            Stack(
              children: [
                Ink.image(
                  image: NetworkImage(
                    'https://s1.bukalapak.com/img/17045341751/large/data.jpeg',
                  ),
                  height: 240,
                  fit: BoxFit.contain,
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    'Erka the original Stethoscope: Rp.2.000.000',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                child: ButtonBar(
              alignment: MainAxisAlignment.start,
            )),
          ],
        ),
      );
}
