from lab_2.models import Notes
from django import forms
from django.forms import fields

class NoteForm(forms.ModelForm):
    class Meta:
        model = Notes
        fields = "__all__"