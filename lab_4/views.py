from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_2.models import Notes
from lab_4.forms import NoteForm
# Create your views here.
def index(request):
    notes = Notes.objects.all().values()
    response = {'notes': notes}
    return render(request, "lab4_index.html" ,response)

def add_note(request):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://127.0.0.1:8000/')
    else:
        form = NoteForm()
    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    notes = Notes.objects.all().values()
    response = {'notes': notes}
    return render(request, "lab4_note_list.html" ,response)



