from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_3.forms import FriendForm
from lab_1.models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    response = {'friends': Friend.objects.all().values()}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def friendform(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('http://127.0.0.1:8000/')
    else:
        form = FriendForm()
    return render(request, 'lab3_form.html', {'form': form})

