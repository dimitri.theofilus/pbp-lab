from django.urls import path
from .views import friendform, index

urlpatterns = [
    path('', index, name='index'),
    path('add_friend', friendform, name='add_friend'),
]