from django.urls import path
from .views import Notes, index, json, xml

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]